var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var fs = require('fs');

const Json2csvParser = require('json2csv').Parser;
const fields = ['fishId', 'bet', 'payout', 'status', 'fishSN'];

function httpGet(theUrl){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}
function regExJwt(sentence){
    const regex = /gameSession":"(.*)"/g;
    var longJwt = sentence.match(regex);
    const shortRegex = /:"(.*)"/g;
    var jwt = longJwt[0].match(shortRegex);
    var jwt = jwt[0].replace( /"/g , "" ).replace( /:/g , "" ) ;
    return jwt;
}

const fishId = 408;
const loopCount = 10;
const bet = 1;
const qps = 1000;
const roomLevel = 0;

var playerId;
var hitResult;
var hitCounter = 0;

var rewardCounter = 0;

let funURL = "https://tw-devcasino.ganapati.tech:7072/tokyokombat/";
let gameId = "tokyokombat";
let url = "https://tw-ws-dev.ganapati.tech";
let socketPort = "10000";
let fullUrl =  url + ':' + socketPort +'/' + gameId;
let jwt = regExJwt(httpGet(funURL));

var io = require('socket.io-client');
socket = io(fullUrl, {query: `session=${jwt}`});
var startTime;

const eventMessages = {
    "Join" : {
        roomLevel: roomLevel
    },
    "Fire" : {
        bulletId: 6,
        bulletX: 1920,
        bulletY: 1080,
        lock: "15-1555426000-1-1-4"
    },
    "Hit" : {
        bulletId: 6,
        bet: bet,
        bulletX: 400,
        bulletY: 300,
        fishes:[
        ]
    },
    "Skill" : {
        bet: 2,
        skillId: 10001
    },
    "SkillHit" : {
        skillId: 10004,
        bulletId: 6,
        bet: bet*30,
        "fishes":[
        ],
    }
};

function hit(){
    startTime = Date.now();
    var prevMiddleTime = startTime;
    var testTimer = setInterval(function(){
        if(hitCounter >= loopCount){
            clearInterval(testTimer);
        }
        let hitData = eventMessages["Hit"];
        hitData.fishes[0] = {
            fishId: fishId,
            fishSN: "QA_Testing_" + hitCounter
        };
        socket.emit("hit", hitData);
        hitCounter ++;
        if (hitCounter % 1000 == 0) {
            let middleTime = Date.now();
            let interval = (middleTime - prevMiddleTime)/1000;
            console.log(`endtime : ${middleTime} ${prevMiddleTime}`);
            console.log(`interval : ${interval}`);
            console.log(`hitCount : ${hitCounter}, QPS : ${1000/interval}hit/s`);
            prevMiddleTime = middleTime;
        }
        if (hitCounter == loopCount + 1) {
            let endTime = Date.now();
            let interval = (endTime - startTime)/1000;
            console.log(`endtime : ${endTime} ${startTime}`);
            console.log(`interval : ${interval}`);
            console.log(`hitCount : ${hitCounter}, QPS : ${loopCount/interval}hit/s`);
        }
    } , 1000/qps);
}

function skillHit(){
    startTime = Date.now();
    var prevMiddleTime = startTime;
    var testTimer = setInterval(function(){
        if(hitCounter >= loopCount){
            clearInterval(testTimer);
        }
        let hitData = eventMessages["SkillHit"];
        hitData.fishes[0] = {
            fishId: fishId,
            fishSN: "QA_Testing_" + hitCounter
        };
        socket.emit("hit", hitData);
        hitCounter ++;
        if (hitCounter % 1000 == 0) {
            let middleTime = Date.now();
            let interval = (middleTime - prevMiddleTime)/1000;
            console.log(`endtime : ${middleTime} ${prevMiddleTime}`);
            console.log(`interval : ${interval}`);
            console.log(`hitCount : ${hitCounter}, QPS : ${1000/interval}hit/s`);
            prevMiddleTime = middleTime;
        }
        if (hitCounter == loopCount + 1) {
            let endTime = Date.now();
            let interval = (endTime - startTime)/1000;
            console.log(`endtime : ${endTime} ${startTime}`);
            console.log(`interval : ${interval}`);
            console.log(`hitCount : ${hitCounter}, QPS : ${loopCount/interval}hit/s`);
        }
    } , 1/qps);
}

socket.on('connect', () => {
    console.log(`socket id : ${socket.id}`);
    console.log(`socket connected : ${socket.connected}`);
    console.log(`socket namespace : ${socket.nsp}`);
    socket.emit('join', eventMessages["Join"]);
});

socket.on('join', data => {
    console.log(`socket join : ${JSON.stringify(data.playerId)}`);
    console.log(`socket join : ${JSON.stringify(data.players)}`);
    playerId =JSON.stringify(data.playerId);
    socket.emit('sync');
    skillHit();
});

socket.on('reward', data => {
    if (JSON.stringify(data.playerId) === playerId) {
        let addData = { 
            fishId : JSON.stringify(data.fishes[0].fishId), 
            bet: bet, 
            payout : JSON.stringify(data.fishes[0].win), 
            status : JSON.stringify(data.fishes[0].status),
            fishSN : data.fishes[0].fishSN
        };
        if (rewardCounter == 0) {
            hitResult = [
                addData
            ];
            rewardCounter ++;
        } else  {
            if (rewardCounter%1000==0) {
                console.log(rewardCounter);
            }
            hitResult.push(addData);
            rewardCounter ++;
        } if(rewardCounter == loopCount + 1) {
            console.log(`writing...`);
            const json2csvParser = new Json2csvParser({ fields, header: true });
            const csv = json2csvParser.parse(hitResult);
            fs.writeFile(`D:/RTP_log/socketio-client/${fishId}_${startTime}.csv`, csv, function(err) {});
            socket.disconnect();
        }
    }
});

// socket.on('appear', data => {
//     console.log(`appear teamId : ${JSON.stringify(data.teamId)} teamType : ${JSON.stringify(data.teamType)}`);
//     console.log(data);
//     socket.disconnect();
//     console.log(`socket connected : ${socket.connected}`);
// });